import React, { Component } from 'react';
import { TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';

class Cell extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.text !== this.props.text;
  }

  render() {
    const { text, onSelect } = this.props;
    return (
      <TouchableOpacity
        onPress={onSelect}
        style={{
          height: 50,
          paddingLeft: 20,
          flex: 1,
          justifyContent: 'center',
        }}
      >
        <Text style={{ color: 'black' }}>{ text }</Text>
      </TouchableOpacity>
    );
  }
}

Cell.propTypes = {
  onSelect: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

export default Cell;
