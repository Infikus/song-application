import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions } from 'react-native';
import { LargeList } from 'react-native-largelist';

import Cell from './Cell.component';

class Songs extends Component {
  constructor(props) {
    super(props);

    const { height } = Dimensions.get('window');
    this.initialNumber = Math.ceil(height / 50);
  }

  shouldComponentUpdate(nextProps) {
    return this.props.songs !== nextProps.songs;
  }

  componentDidUpdate() {
    this.largeList.reloadData();
  }

  render() {
    return (
      <LargeList
        ref={(ref) => { this.largeList = ref; }}
        style={{ flex: 1 }}
        numberOfRowsInSection={() => this.props.songs.length}
        renderCell={(sectionIndex, rowIndex) => (
          <Cell
            text={this.props.songs[rowIndex].title}
            onSelect={() => this.props.onSelect(this.props.songs[rowIndex])}
          />
        )}
        heightForCell={() => 50}
      />
    );
  }
}

Songs.propTypes = {
  songs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
  })).isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default Songs;
