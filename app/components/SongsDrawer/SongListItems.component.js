import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions } from 'react-native';
import { LargeList } from 'react-native-largelist';

import Cell from './Cell.component';

class SongListItems extends Component {
  constructor(props) {
    super(props);

    const { height } = Dimensions.get('window');
    this.initialNumber = Math.ceil(height / 50);
  }

  shouldComponentUpdate(nextProps) {
    return this.props.songListItems !== nextProps.songListItems;
  }

  componentDidUpdate() {
    this.largeList.reloadData();
  }

  render() {
    const { songListItems, onSelect } = this.props;
    return (
      <LargeList
        ref={(ref) => { this.largeList = ref; }}
        style={{ flex: 1 }}
        numberOfRowsInSection={() => songListItems.length}
        renderCell={(sectionIndex, rowIndex) => (
          <Cell
            text={songListItems[rowIndex].song.title}
            onSelect={() => onSelect(songListItems[rowIndex])}
          />
        )}
        heightForCell={() => 50}
      />
    );
  }
}

SongListItems.propTypes = {
  songListItems: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    position: PropTypes.number.isRequired,
    song: PropTypes.shape({
      id: PropTypes.number.isRequired,
      content: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default SongListItems;
