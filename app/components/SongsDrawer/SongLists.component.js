import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Dimensions } from 'react-native';
import { LargeList } from 'react-native-largelist';

import Cell from './Cell.component';

class SongLists extends Component {
  constructor(props) {
    super(props);

    const { height } = Dimensions.get('window');
    this.initialNumber = Math.ceil(height / 50);
  }

  shouldComponentUpdate(nextProps) {
    return this.props.songLists !== nextProps.songLists;
  }

  componentDidUpdate() {
    this.largeList.reloadData();
  }

  render() {
    const { songLists, onSelect } = this.props;
    return (
      <LargeList
        ref={(ref) => { this.largeList = ref; }}
        style={{ flex: 1 }}
        numberOfRowsInSection={() => songLists.length}
        renderCell={(sectionIndex, rowIndex) => (
          <Cell
            text={songLists[rowIndex].name}
            onSelect={() => onSelect(songLists[rowIndex])}
          />
        )}
        heightForCell={() => 50}
      />
    );
  }
}

SongLists.propTypes = {
  songLists: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
  })).isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default SongLists;
