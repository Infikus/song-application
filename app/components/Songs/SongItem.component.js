import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import SongContent from './SongContent.component';

class SongItem extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.song.id !== this.props.song.id ||
      nextProps.width !== this.props.width ||
      nextProps.capo !== this.props.capo;
  }

  render() {
    const {
      width,
      capo,
      song,
    } = this.props;
    return (
      <View
        style={{ flex: 1, width }}
      >
        <SongContent
          capo={capo}
          key={song.id}
          song={song}
        />
      </View>
    );
  }
}

SongItem.propTypes = {
  width: PropTypes.number.isRequired,
  capo: PropTypes.number.isRequired,
  song: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }).isRequired,
};

export default SongItem;
