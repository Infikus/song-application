import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, FlatList, View } from 'react-native';
import SongItem from './SongItem.component';

class Songs extends Component {
  static getSongCapo(song) {
    if (!song) return 0;
    if (!song.song_params || song.song_params.length === 0) return 0;
    return song.song_params[0].capo;
  }

  constructor(props) {
    super(props);
    this.swiper = null;
    this.state = {
      width: Dimensions.get('window').width,
      currentIdSwiper: this.calculateSlideId(),
    };
    this.onIndexChanged = this.onIndexChanged.bind(this);
    this.onScrollEnd = this.onScrollEnd.bind(this);
    this.swiperScrollTo = this.swiperScrollTo.bind(this);
    this.updateWidth = this.updateWidth.bind(this);
    this.getItemLayout = this.getItemLayout.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // TODO: Corriger les "rollback" quand on slide trop vite
    if (nextProps.selectedSongId !== this.props.selectedSongId) {
      const slideId = this.calculateSlideId(nextProps);
      this.swiperScrollTo(slideId);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    // return !areSameSongArray(nextProps.songs, this.props.songs);
    return nextProps.songs !== this.props.songs || this.state.width !== nextState.width;
  }

  onIndexChanged(index) {
    this.setState({
      currentIdSwiper: index,
    }, () => this.props.onIndexChanged(index));
  }

  onScrollEnd(e) {
    const { contentOffset } = e.nativeEvent;
    const viewSize = e.nativeEvent.layoutMeasurement;

    // Divide the horizontal offset by the width of the view to see which page is visible
    const pageNum = Math.floor(contentOffset.x / viewSize.width);
    if (pageNum !== this.state.currentIdSwiper) {
      this.onIndexChanged(pageNum);
    }
  }

  // eslint-disable-next-line
  getItemLayout(data, index) {
    const { width } = this.state;
    return { length: width, offset: width * index, index };
  }

  swiperScrollTo(index) {
    return this.flatList.scrollToIndex({ index });
  }

  calculateSlideId(props = this.props) {
    return props.songs.findIndex(e => e.id === props.selectedSongId);
  }

  updateWidth() {
    this.setState({
      width: Dimensions.get('window').width,
    });
  }

  renderSwiper() {
    const { songs } = this.props;
    const { currentIdSwiper, width } = this.state;
    return (
      <FlatList
        key={width}
        ref={(l) => { this.flatList = l; }}
        getItemLayout={this.getItemLayout}
        horizontal
        pagingEnabled
        keyExtractor={item => item.id}
        data={songs}
        onMomentumScrollEnd={this.onScrollEnd}
        initialScrollIndex={currentIdSwiper}
        removeClippedSubviews
        extraData={this.state}
        initialNumToRender={1}
        renderItem={({ item }) => (
          <SongItem
            width={width}
            capo={Songs.getSongCapo(item)}
            song={item}
          />
       )}
      />
    );
  }

  render() {
    return (
      <View
        style={{ flex: 1 }}
        onLayout={this.updateWidth}
      >
        { this.renderSwiper() }
      </View>
    );
  }
}

Songs.propTypes = {
  onIndexChanged: PropTypes.func.isRequired,
  songs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    content: PropTypes.string.isRequired,
  })).isRequired,
  selectedSongId: PropTypes.number,
};

Songs.defaultProps = {
  selectedSongId: undefined,
};

export default Songs;
