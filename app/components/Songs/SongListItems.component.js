import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FlatList, Dimensions, View } from 'react-native';

import { areSameSongListItemsArray } from '../../screens/utils';
import SongItem from './SongItem.component';

class SongListItems extends Component {
  static getSongListItemCapo(item) {
    if (!item) return 0;
    if (item.song_params && item.song_params.length > 0) {
      return item.song_params[0].capo;
    }
    if (item.song && item.song.song_params && item.song.song_params.length > 0) {
      return item.song.song_params[0].capo;
    }
    return 0;
  }

  constructor(props) {
    super(props);
    this.swiper = null;
    this.state = {
      width: Dimensions.get('window').width,
      currentIdSwiper: this.calculateSlideId(),
    };
    this.onIndexChanged = this.onIndexChanged.bind(this);
    this.updateWidth = this.updateWidth.bind(this);
    this.getItemLayout = this.getItemLayout.bind(this);
    this.onScrollEnd = this.onScrollEnd.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // TODO: Corriger les "rollback" quand on slide trop vite
    if (nextProps.selectedSongId !== this.props.selectedSongId) {
      const slideId = this.calculateSlideId(nextProps);
      this.swiperScrollTo(slideId);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !areSameSongListItemsArray(this.props.songListItems, nextProps.songListItems) ||
      this.state.width !== nextState.width;
  }

  onIndexChanged(index) {
    this.setState({
      currentIdSwiper: index,
    }, () => this.props.onIndexChanged(index));
  }

  onScrollEnd(e) {
    const { contentOffset } = e.nativeEvent;
    const viewSize = e.nativeEvent.layoutMeasurement;

    // Divide the horizontal offset by the width of the view to see which page is visible
    const pageNum = Math.floor(contentOffset.x / viewSize.width);
    if (pageNum !== this.state.currentIdSwiper) {
      this.onIndexChanged(pageNum);
    }
  }

  // eslint-disable-next-line
  getItemLayout(data, index) {
    const { width } = this.state;
    return { length: width, offset: width * index, index };
  }

  calculateSlideId(props = this.props) {
    const item = props.songListItems.find(e => e.id === props.selectedSongId);
    return item.position;
  }

  swiperScrollTo(index) {
    this.flatList.scrollToIndex({ index });
    this.setState({
      currentIdSwiper: index,
    });
  }

  updateWidth() {
    this.setState({
      width: Dimensions.get('window').width,
    });
  }

  renderSwiper() {
    const { songListItems } = this.props;
    const { currentIdSwiper, width } = this.state;
    // The key is used to force a re-render after an orientation change
    return (
      <FlatList
        key={width}
        ref={(l) => { this.flatList = l; }}
        getItemLayout={this.getItemLayout}
        horizontal
        pagingEnabled
        keyExtractor={item => item.id}
        data={songListItems}
        onMomentumScrollEnd={this.onScrollEnd}
        initialScrollIndex={currentIdSwiper}
        removeClippedSubviews
        initialNumToRender={1}
        renderItem={({ item }) => (
          <SongItem
            width={width}
            capo={SongListItems.getSongListItemCapo(item)}
            song={item.song}
          />
        )}
      />
    );
  }

  render() {
    return (
      <View
        onLayout={this.updateWidth}
        style={{ flex: 1 }}
      >
        { this.renderSwiper() }
      </View>
    );
  }
}

SongListItems.propTypes = {
  onIndexChanged: PropTypes.func.isRequired,
  songListItems: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    song: PropTypes.shape({
      id: PropTypes.number.isRequired,
      content: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
  selectedSongId: PropTypes.number.isRequired,
};

export default SongListItems;
