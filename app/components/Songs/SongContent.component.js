import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { WebView } from 'react-native';

const songStyle =
`<style>
#song {
  position: relative;
  line-height: 150%;
  margin-left: -1em;
  padding-left: 1em;
}

#song .phrase-style > .chord {
  word-wrap: normal;
  white-space: nowrap;
  color: red;
  line-height: 250%;
  vertical-align: bottom;
  display: inline-block;
  position: relative;
  font-weight: bold;
  top:-8px;
  width: 0;
}

#song .comment {
  color: grey;
}

#song .chorus {
  border-left: 3px solid black;
  padding-left: 1em;
}

#header {
  margin-bottom: 1em;
}

#header .title {
  display: block;
  font-size: 1.4em;
}

#header .capo {
  background-color: #AAA;
  font-size: 0.9em;
}
</style>`;

const htmlTemplate =
`<html>
  <head>
  <meta id="viewport" name="viewport" content="user-scalable=no" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  ${songStyle}
  </head>
  <body> 
    ____CONTENT____
  </body>
</html>`;

class SongContent extends Component {
  static chordToNumber(letter, alt) {
    let res = 0;
    switch (letter) {
      case 'A': res = 0; break;
      case 'B': res = 2; break;
      case 'C': res = 3; break;
      case 'D': res = 5; break;
      case 'E': res = 7; break;
      case 'F': res = 8; break;
      case 'G': res = 10; break;
      default: return 0;
    }
    if (alt === '#') {
      res += 1;
    } else if (alt === 'b') {
      res -= 1;
    }
    return (res + 12) % 12;
  }

  static numberToChord(val) {
    switch ((val + 12) % 12) {
      case 0: return 'A';
      case 1: return 'Bb';
      case 2: return 'B';
      case 3: return 'C';
      case 4: return 'C#';
      case 5: return 'D';
      case 6: return 'Eb';
      case 7: return 'E';
      case 8: return 'F';
      case 9: return 'F#';
      case 10: return 'G';
      case 11: return 'G#';
      default: return '';
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      contentHtml: this.transformContent(this.props.song, this.props.capo),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.song.content !== this.props.song.content || this.props.capo !== nextProps.capo) {
      this.setState({
        contentHtml: this.transformContent(nextProps.song, nextProps.capo),
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.song !== nextProps.song || this.props.capo !== nextProps.capo) {
      return true;
    } else if (this.state !== nextState) {
      return true;
    }
    return false;
  }

  // eslint-disable-next-line class-methods-use-this
  chordMatchToHtml(c, p1, chord, content, capo) {
    let nChord = chord;
    if (nChord && capo > 0) {
      const chordRegex = /(A|B|C|D|E|F|G)(#|b)?(.*)/;
      nChord = chord.replace(chordRegex, (correspondance, letter, alt, rest) => {
        const numberVal = SongContent.chordToNumber(letter, alt);
        const chordWithCapo = SongContent.numberToChord(numberVal - capo);
        return `${chordWithCapo}${rest}`;
      });
    }
    nChord = nChord !== undefined ? nChord : '';
    return `<span class='phrase'><span class='chord'>${nChord}</span><span class='content'>${content}</span></span>`;
  }

  // eslint-disable-next-line class-methods-use-this
  transformContent(song, capo) {
    let result = song.content;
    const chordRegex = /(\[([^\]\n\t]*)\])?([^[\n\t]*)/g;
    const commentRegex = /\{(c|comment):([^}{\n\t]*)\}/g;
    const chorusRegex = /\{(soc|start_of_chorus)\}\n?([^]*?)\{(eoc|end_of_chorus)\}/g;

    const brRegex = /([\n])/g;

    result = result.replace(
      chordRegex,
      (c, p1, chord, cont) => this.chordMatchToHtml(c, p1, chord, cont, capo),
    );
    result = result.replace(commentRegex, "<span class='comment'>$2</span>");
    result = result.replace(chorusRegex, "<div class='chorus'>$2</div>");

    result = result.replace(brRegex, '<br/>');
    const capoStr = capo !== 0 ? `<span class="capo">Capo ${capo}</span>` : '';
    const titleStr = `<h1 class="title">${song.title}</h1>`;
    return htmlTemplate.replace(
      '____CONTENT____',
      `<div id='debug'></div>
      <div id='header'>${titleStr}${capoStr}</div>
      <div id='song'>${result}</div>`,
    );
  }

  // eslint-disable-next-line class-methods-use-this
  preventChordOverlapping() {
    const phrases = document.getElementsByClassName('phrase');
    const minSpace = 10; // Minimal space between two chords
    for (let i = 0; i < phrases.length; i += 1) {
      const chord = phrases[i].getElementsByClassName('chord')[0];
      const content = phrases[i].getElementsByClassName('content')[0];
      const chordBox = chord.getBoundingClientRect();
      const wChord = chordBox.right - chordBox.left;
      const contentBox = content.getBoundingClientRect();
      const wContent = contentBox.right - contentBox.left;

      if (wChord + minSpace > wContent) {
        content.style.marginRight = `${-wContent + minSpace + wChord}px`;
      }

      phrases[i].className += ' phrase-style';
    }
  }

  render() {
    return (
      <WebView
        source={{
          html: this.state.contentHtml,
          baseUrl: '',
        }}
        injectedJavaScript={`${this.preventChordOverlapping.toString()} preventChordOverlapping();`}
        automaticallyAdjustContentInsets
        scalesPageToFit
      />
    );
  }
}

SongContent.propTypes = {
  song: PropTypes.shape({
    id: PropTypes.number.isRequired,
    content: PropTypes.string.isRequired,
  }).isRequired,
  capo: PropTypes.number,
};

SongContent.defaultProps = {
  capo: 0,
};

export default SongContent;
