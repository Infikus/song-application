import React from 'react';
import PropTypes from 'prop-types';
import { Text, ScrollView, View, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { Button, List, ListItem, H1 } from 'native-base';

import { numberToChord } from '../../utils/chords';

const CapoModal = ({
  songKey,
  capo,
  visible,
  onCapoChange,
  onClose,
}) => (
  <Modal
    isVisible={visible}
    onBackdropPress={onClose}
  >
    <View
      style={{
        backgroundColor: 'white',
        width: 250,
        marginLeft: 'auto',
        marginRight: 'auto',
        padding: 22,
        paddingTop: 44,
        paddingBottom: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
      }}
    >
      <H1>Capodastre</H1>
      <ScrollView>
        <List style={{ width: 250 }}>
          {
            Array.from({ length: 13 }, (x, i) => i).map((val) => {
              const newKey = numberToChord(songKey - val);
              return (
                <ListItem key={val}>
                  <TouchableOpacity
                    onPress={() => onCapoChange(val)}
                    style={{
                      flex: 1,
                    }}
                  >
                    <Text>{`capo ${val}${val === capo ? ' (Selectionné)' : `(${newKey})`}`}</Text>
                  </TouchableOpacity>
                </ListItem>
              );
            })
          }
        </List>
      </ScrollView>
      <Button
        onPress={onClose}
        style={{
          marginLeft: 'auto',
          marginRight: 'auto',
          marginTop: 22,
        }}
      >
        <Text>Annuler</Text>
      </Button>
    </View>
  </Modal>
);

CapoModal.propTypes = {
  songKey: PropTypes.number,
  capo: PropTypes.number,
  visible: PropTypes.bool,
  onCapoChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

CapoModal.defaultProps = {
  songKey: 0,
  capo: 0,
  visible: false,
};

export default CapoModal;
