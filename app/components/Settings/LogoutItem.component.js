import React from 'react';
import PropTypes from 'prop-types';
import { ListItem, Left, Icon, Body, Text } from 'native-base';

const LogoutItem = ({
  onPress,
}) => (
  <ListItem
    icon
    onPress={onPress}
  >
    <Left>
      <Icon active name="ios-log-out" />
    </Left>
    <Body>
      <Text>Deconnexion</Text>
    </Body>
  </ListItem>
);

LogoutItem.propTypes = {
  onPress: PropTypes.func.isRequired,
};

export default LogoutItem;
