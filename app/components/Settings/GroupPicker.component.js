import React from 'react';
import PropTypes from 'prop-types';
import { ListItem, Left, Icon, Body, Text, Right, Picker, Item } from 'native-base';

const GroupPicker = ({
  teams,
  onTeamSelect,
  current,
}) => (
  <ListItem icon>
    <Left>
      <Icon active name="chatboxes" />
    </Left>
    <Body>
      <Text>Groupe</Text>
    </Body>
    <Right>
      <Picker
        mode="dialog"
        iosHeader="Selection du groupe"
        selectedValue={current}
        onValueChange={onTeamSelect}
      >
        {
          teams.map(team => (
            <Item
              key={team.id}
              label={team.name}
              value={team.id}
            />
          ))
        }
      </Picker>
    </Right>
  </ListItem>
);

GroupPicker.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  onTeamSelect: PropTypes.func.isRequired,
  current: PropTypes.number,
};

GroupPicker.defaultProps = {
  current: undefined,
};

export default GroupPicker;
