import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import Yup from 'yup';
import { Form, Item, Input, Button, Text } from 'native-base';

const validationSchema = Yup.object().shape({
  email: Yup.string().email('Adresse mail invalide').required(''),
  password: Yup.string().min(8, '8 caractères minimum').required(''),
});

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submitted: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit() {
    this.setState({
      submitted: true,
    });
  }

  render() {
    const { onSubmit } = this.props;
    const { submitted } = this.state;
    return (
      <Formik
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        initialValues={{
          email: 'test@test.test',
          password: 'testtest',
        }}
        render={
          ({
            values,
            errors,
            handleSubmit,
            setFieldValue,
          }) => (
            <Form>
              <Item
                error={!!errors.email && submitted}
              >
                <Input
                  placeholder="Adresse mail"
                  value={values.email}
                  onChangeText={text => setFieldValue('email', text)}
                />
              </Item>
              <Item
                error={!!errors.password && submitted}
              >
                <Input
                  placeholder="Mot de passe"
                  secureTextEntry
                  value={values.password}
                  onChangeText={text => setFieldValue('password', text)}
                />
              </Item>
              <Button
                onPress={(e) => { this.onSubmit(); handleSubmit(e); }}
              >
                <Text>Connexion</Text>
              </Button>
            </Form>
          )
        }
      />
    )
  }
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  loginPending: PropTypes.bool,
};

LoginForm.defaultProps = {
  loginPending: false,
};

export default LoginForm;
