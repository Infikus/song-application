import { createSelectorCreator, defaultMemoize } from 'reselect';
import { isEqual } from 'lodash';
import { getSelectedTeam } from '../rootSelectors';

// create a "selector creator" that uses lodash.isEqual instead of ===
const createDeepEqualSelector = createSelectorCreator(
  defaultMemoize,
  isEqual,
);


export const getSelectedTeamSongs = createDeepEqualSelector(
  [getSelectedTeam],
  (team) => {
    console.log('getSelectedSongs');
    if (!team || !team.songs) return [];
    const data = team.songs;

    return data;
  },
);

export const blabla = 0;
