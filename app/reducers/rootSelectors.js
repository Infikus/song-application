import * as songsEntityReducer from './entities/songs.reducer';
import * as meAppReducer from './app/me.reducer';
import * as songListsEntityReducer from './entities/songLists.reducer';
import * as songListItemsEntityReducer from './entities/songListItems.reducer';
import * as pendingAppReducer from './app/pending.reducer';
import * as drawerAppReducer from './app/drawer.reducer';

export const isPending = (action, state) =>
  pendingAppReducer.isPending(action, state.app.pending);
export const isSuccessful = (action, state) =>
  pendingAppReducer.isSuccessful(action, state.app.pending);
export const isError = (action, state) =>
  pendingAppReducer.isError(action, state.app.pending);

export const getSong = (id, state) =>
  songsEntityReducer.getSong(id, state.entities);
export const getAllSongs = state =>
  songsEntityReducer.getAllSongs(state.entities);

export const getSongList = (id, state, denorm = true) =>
  songListsEntityReducer.getSongList(id, state.entities, denorm);
export const getSongsOfSongList = (id, state) =>
  songListsEntityReducer.getSongsOfSongList(id, state.entities);

export const getSongListItemsOfSongList = (id, state) =>
  songListItemsEntityReducer.getBySongList(id, state.entities);

export const getMe = state =>
  meAppReducer.getMe(state.app.me, state.entities);
export const getAccessToken = state =>
  meAppReducer.getAccessToken(state.app.me);
export const getRefreshToken = state =>
  meAppReducer.getRefreshToken(state.app.me);
export const getMyTeams = state =>
  meAppReducer.getMyTeams(state.app.me, state.entities);
export const getSelectedSongId = state =>
  meAppReducer.getSelectedSongId(state.app.me);
export const getSelectedSongListId = state =>
  meAppReducer.getSelectedSongListId(state.app.me);
export const isSongSelected = state =>
  meAppReducer.isSongSelected(state.app.me);
export const getSelectedTeam = state =>
  meAppReducer.getSelectedTeam(state.app.me, state.entities);
export const getSelectedTeamId = state =>
  meAppReducer.getSelectedTeamId(state.app.me);
export const getSelectedTeamSongs = state =>
  meAppReducer.getSelectedTeamSongs(state.app.me, state.entities);
export const getSelectedTeamSongLists = state =>
  meAppReducer.getSelectedTeamSongLists(state.app.me, state.entities);
export const isMePending = state =>
  meAppReducer.isPending(state.app.me);
export const isTokenRefreshing = state =>
  meAppReducer.isTokenRefreshing(state.app.me);
export const getWaitingActions = state =>
  meAppReducer.getWaitingActions(state.app.me);

export const isSongsDrawerOpen = state =>
  drawerAppReducer.isSongsDrawerOpen(state.app.drawer);
