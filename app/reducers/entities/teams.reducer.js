import { denormalize } from 'normalizr';
import { teamSchema } from '../../sagas/api/schemas';
import * as songActions from '../../actions/songs.action';
import * as userActions from '../../actions/users.action';
import * as songListActions from '../../actions/songLists.action';
import { getSong } from './songs.reducer';

const initialState = {};

function teams(state = initialState, action) {
  switch (action.type) {
    case userActions.ME_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.teams,
      };
    case songActions.FETCH_SONGS_SUCCESS: {
      const teamId = Object.keys(action.payload.entities.teams)[0];
      return {
        ...state,
        [teamId]: {
          ...state[teamId],
          songs: action.payload.songsId,
        },
      };
    }
    case songListActions.FETCH_SONG_LISTS_SUCCESS: {
      const teamId = Object.keys(action.payload.entities.teams)[0];
      return {
        ...state,
        [teamId]: {
          ...state[teamId],
          song_lists: action.payload.songListsId,
        },
      };
    }

    case userActions.LOGOUT:
      return initialState;

    default:
      return state;
  }
}

export const getTeam = (id, state) =>
  denormalize(id, teamSchema, state);

export const getTeamSongs = (teamId, state) => {
  if (!state.teams[teamId] || !state.teams[teamId].songs) {
    return [];
  }
  const songs = state.teams[teamId].songs
    .map(id => getSong(id, state))
    .sort((a, b) => {
      if (a.title.toLowerCase() > b.title.toLowerCase()) {
        return 1;
      }
      return -1;
    });
  return songs;
};

export const getTeamSongLists = (teamId, state) => {
  if (!state.teams[teamId] || !state.teams[teamId].song_lists) {
    return [];
  }
  return state.teams[teamId].song_lists.map(id => state.song_lists[id]);
};

export default teams;
