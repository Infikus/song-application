import reducer from './songListItems.reducer';
import * as actions from '../../actions/songLists.action';

describe('songListItems reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({});
  });

  it('should handle FETCH_SONG_LISTS_SUCCESS', () => {
    const songListItems = {
      1: {
        name: 'name 1',
      },
      3: {
        name: 'name 3',
      },
    };
    const state = {
      1: {
        name: 'initial name 1',
      },
      2: {
        name: 'name 2',
      },
    };

    expect(reducer(state, actions.fetchSongListsSuccess([1, 3], {
      song_list_items: songListItems,
    })))
      .toEqual({
        ...state,
        ...songListItems,
      });
  });

  it('should hendle FETCH_SONG_LIST_SUCCESS', () => {
    const songListItems = {
      1: {
        name: 'name 1',
      },
    };
    const state = {
      1: {
        name: 'initial name 1',
      },
      2: {
        name: 'name 2',
      },
    };

    expect(reducer(state, actions.fetchSongListsSuccess(1, {
      song_list_items: songListItems,
    })))
      .toEqual({
        ...state,
        ...songListItems,
      });
  });
});
