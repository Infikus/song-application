import { denormalize } from 'normalizr';
import * as actions from '../../actions/songLists.action';
import { songListSchema } from '../../sagas/api/schemas';
import { LOGOUT } from '../../actions/users.action';

const initialState = {};

function songLists(state = initialState, action) {
  switch (action.type) {
    case actions.FETCH_SONG_LISTS_SUCCESS:
    case actions.FETCH_SONG_LIST_SUCCESS: {
      const { entities } = action.payload;
      return {
        ...state,
        ...entities.song_lists,
      };
    }

    case LOGOUT:
      return initialState;

    default:
      return state;
  }
}

export const getSongList = (id, state, denorm = true) => {
  if (denorm) {
    return denormalize(id, songListSchema, state);
  }
  return state.song_lists[id];
};

// Return all the songs in a list, ordered by position
export const getSongsOfSongList = (id, state) => {
  if (id === undefined) {
    return [];
  }
  const itemsId = state.song_lists[id] ? state.song_lists[id].song_list_items : [];
  const res = itemsId
    .map((itemId) => {
      const songListItem = state.song_list_items[itemId];
      return {
        ...songListItem,
        song: state.songs[songListItem.song],
      };
    })
    .sort((item1, item2) => item1.position > item2.position)
    .map(item => item.song);
  return res;
};

export default songLists;
