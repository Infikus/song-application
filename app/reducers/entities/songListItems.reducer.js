import { denormalize } from 'normalizr';
import { songListItemSchema } from '../../sagas/api/schemas';
import * as actions from '../../actions/songLists.action';
import LOGOUT from '../../actions/users.action';

const initialState = {};

function songListItems(state = initialState, action) {
  switch (action.type) {
    case actions.FETCH_SONG_LISTS_SUCCESS:
    case actions.FETCH_SONG_LIST_SUCCESS: {
      const { entities } = action.payload;
      return {
        ...state,
        ...entities.song_list_items,
      };
    }

    case LOGOUT:
      return initialState;

    default:
      return state;
  }
}

export const getBySongList = (songListId, entitiesState) => {
  if (songListId === undefined) return [];

  const songList = entitiesState.song_lists[songListId];
  if (!songList) return [];

  const data = denormalize(songList.song_list_items, [songListItemSchema], entitiesState);
  return data.map(item => ({
    ...item,
    song_list: songListId,
  })).sort((i1, i2) => i1.position < i2.position ? -1 : 1); // eslint-disable-line
};

export default songListItems;
