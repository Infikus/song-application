import { denormalize } from 'normalizr';
import * as userActions from '../../actions/users.action';
import { teamMemberSchema } from '../../sagas/api/schemas';

export const roleToString = (role) => {
  switch (role) {
    case 0: return 'Administrateur';
    case 1: return 'Membre';
    default: return 'Indéfini';
  }
};

const initialState = {};

function teamMembers(state = initialState, action) {
  switch (action.type) {
    case userActions.ME_SUCCESS: {
      const { entities } = action.payload;
      return {
        ...state,
        ...entities.team_members,
      };
    }

    case userActions.LOGOUT:
      return initialState;

    default:
      return state;
  }
}

export const getTeamMember = (id, state) =>
  denormalize(id, teamMemberSchema, state);

export default teamMembers;
