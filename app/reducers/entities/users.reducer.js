import { denormalize } from 'normalizr';
import { userSchema } from '../../sagas/api/schemas';
import * as userActions from '../../actions/users.action';

const initialState = {};

function users(state = initialState, action) {
  switch (action.type) {
    case userActions.ME_SUCCESS: {
      const { entities } = action.payload;
      return {
        ...state,
        ...entities.users,
      };
    }

    case userActions.LOGOUT:
      return initialState;

    default:
      return state;
  }
}

export const getUser = (id, state) =>
  denormalize(id, userSchema, state);

export default users;
