import { denormalize } from 'normalizr';
import * as actions from '../../actions/songs.action';
import * as songListActions from '../../actions/songLists.action';
import { songSchema } from '../../sagas/api/schemas';
import { LOGOUT } from '../../actions/users.action';

const initialState = {};

function songs(state = initialState, action) {
  switch (action.type) {
    case actions.FETCH_SONGS_SUCCESS:
    case actions.FETCH_SONG_SUCCESS:
    case songListActions.FETCH_SONG_LIST_SUCCESS:
    case songListActions.FETCH_SONG_LISTS_SUCCESS: {
      const { entities } = action.payload;
      return {
        ...state,
        ...entities.songs,
      };
    }

    case LOGOUT:
      return initialState;

    default:
      return state;
  }
}

export const getSong = (id, state) =>
  denormalize(id, songSchema, state);

// Return all songs ordered by name
export const getAllSongs = state =>
  state.songs;

export default songs;
