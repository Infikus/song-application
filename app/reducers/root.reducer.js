import { combineReducers } from 'redux';

import songListItemsEntities from './entities/songListItems.reducer';
import songListsEntities from './entities/songLists.reducer';
import songsEntities from './entities/songs.reducer';
import songParamsEntities from './entities/songParams.reducer';
import teamsEntities from './entities/teams.reducer';
import teamMembersEntities from './entities/teamMembers.reducer';
import usersEntities from './entities/users.reducer';

import meApp from './app/me.reducer';
import pendingApp from './app/pending.reducer';
import drawerApp from './app/drawer.reducer';

const entitiesReducer = combineReducers({
  song_list_items: songListItemsEntities,
  song_lists: songListsEntities,
  songs: songsEntities,
  song_params: songParamsEntities,
  teams: teamsEntities,
  team_members: teamMembersEntities,
  users: usersEntities,
});

const appReducer = combineReducers({
  me: meApp,
  pending: pendingApp,
  drawer: drawerApp,
});

const rootReducer = combineReducers({
  entities: entitiesReducer,
  app: appReducer,
});

export default rootReducer;
