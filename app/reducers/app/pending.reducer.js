import { LOGOUT } from '../../actions/users.action';

const initialState = {};

function pending(state = initialState, action) {
  const requestRegex = /(.*)_REQUEST/;
  const successRegex = /(.*)_SUCCESS/;
  const failureRegex = /(.*)_FAILURE/;
  let actionName = null;

  actionName = action.type.match(requestRegex);
  if (actionName) {
    return {
      ...state,
      [actionName[1]]: {
        pending: true,
        error: null,
        success: false,
      },
    };
  }

  actionName = action.type.match(successRegex);
  if (actionName) {
    return {
      ...state,
      [actionName[1]]: {
        pending: false,
        error: null,
        success: true,
      },
    };
  }

  actionName = action.type.match(failureRegex);
  if (actionName) {
    return {
      ...state,
      [actionName[1]]: {
        pending: false,
        error: action.payload.error,
        success: false,
      },
    };
  }

  if (actionName === LOGOUT) {
    return initialState;
  }

  return state;
}

export function isPending(action, state) {
  const actionState = state[action];
  return actionState ? actionState.pending : false;
}

export function isSuccessful(action, state) {
  const actionState = state[action];
  return actionState ? actionState.success : false;
}

export function isError(action, state) {
  const actionState = state[action];
  return actionState ? actionState.error : null;
}

export default pending;
