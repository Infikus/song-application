import * as actions from '../../actions/users.action';
import * as songsActions from '../../actions/songs.action';
import { getUser } from '../entities/users.reducer';
import { getTeam, getTeamSongs, getTeamSongLists } from '../entities/teams.reducer';


const initialState = {
  userId: undefined,
  accessToken: undefined,
  refreshToken: undefined,

  selectedTeam: undefined,
  selectedSong: undefined,
  selectedSongList: undefined,
  isSongSelected: false,

  pending: false,

  isTokenRefreshing: false,
  waitingActions: [],

  registerSuccess: false,
};

function updateSelectedTeam(state, selectedTeam) {
  // Update only if currentSelectedTeam is undefined
  return state.selectedTeam ? state.selectedTeam : selectedTeam;
}

function updateSelectedSong(currentSelectedSong, songsId, songs) {
  if (currentSelectedSong !== undefined) {
    return currentSelectedSong;
  } else if (songsId.length === 0) {
    return undefined;
  }
  return songsId.reduce((acc, songId) => {
    if (songs[songId].title < acc.title) {
      return songs[songId];
    }
    return acc;
  }, songs[songsId[0]]).id;
}

function me(state = initialState, action) {
  switch (action.type) {
    case actions.ME_SUCCESS: {
      const { userId, entities } = action.payload;
      const selectedTeamMember = entities.users[userId].last_team_member;
      const selectedTeamId = entities.team_members[selectedTeamMember].team;
      return {
        ...state,
        userId: action.payload.userId,
        selectedTeam: updateSelectedTeam(state, selectedTeamId),
      };
    }

    case actions.SET_SELECTED_TEAM:
      return {
        ...state,
        selectedTeam: action.payload.teamId,
        selectedSong: undefined,
        selectedSongList: undefined,
      };

    case actions.SET_SELECTED_SONG:
      return {
        ...state,
        selectedSong: action.payload.songId,
        selectedSongList: action.payload.songListId,
        isSongSelected: true,
      };

    case actions.SET_SELECTED_SONG_DONE:
      return {
        ...state,
        isSongSelected: false,
      };

    case songsActions.FETCH_SONGS_SUCCESS:
      return {
        ...state,
        selectedSong: updateSelectedSong(
          state.selectedSong,
          action.payload.songsId,
          action.payload.entities.songs,
        ),
      };

    case actions.LOGIN_SUCCESS:
      return {
        ...state,
        selectedTeam: null,
        accessToken: action.user.token,
        refreshToken: action.user.refresh_token,
      };

    // Refresh token handling
    case actions.REFRESH_TOKEN_REQUEST:
      return {
        ...state,
        isTokenRefreshing: true,
      };

    case actions.ADD_WAITING_ACTION:
      return {
        ...state,
        waitingActions: [...state.waitingActions, action.payload.action],
      };

    case actions.REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        accessToken: action.payload.token,
        isTokenRefreshing: false,
        waitingActions: [],
      };

    case actions.LOGOUT:
      return initialState;

    default:
      return state;
  }
}

export function getMe(state, entitiesState) {
  return getUser(state.userId, entitiesState);
}
export function getAccessToken(state) {
  return state.accessToken;
}
export function getRefreshToken(state) {
  return state.refreshToken;
}
export function getMyTeams(state, entitiesState) {
  return getMe(state, entitiesState).team_members.map(tm => tm.team);
}
export function getSelectedSongId(state) {
  return state.selectedSong;
}
export function getSelectedSongListId(state) {
  return state.selectedSongList;
}
export function getSelectedTeamId(state) {
  return state.selectedTeam;
}
export function getSelectedTeam(state, entitiesState) {
  return getTeam(state.selectedTeam, entitiesState);
}
export function getSelectedTeamSongs(state, entitiesState) {
  return getTeamSongs(state.selectedTeam, entitiesState);
}
export function isSongSelected(state) {
  return state.isSongSelected;
}
export function getSelectedTeamSongLists(state, entitiesState) {
  return getTeamSongLists(state.selectedTeam, entitiesState);
}
/**
 * Return the role of the user if he is in the team, else -1
 */
export function getRoleInTeam(teamId, state, entitiesState) {
  const { userId } = state;
  const teamMemberId = Object.keys(entitiesState.team_members)
    .find(id =>
      parseInt(entitiesState.team_members[id].user, 10) === parseInt(userId, 10) &&
      parseInt(entitiesState.team_members[id].team, 10) === parseInt(teamId, 10));

  if (teamMemberId === undefined) {
    return -1;
  }
  const teamMember = entitiesState.team_members[teamMemberId];
  return teamMember.role;
}

export function isTokenRefreshing(state) {
  return state.isTokenRefreshing;
}
export function getWaitingActions(state) {
  return state.waitingActions;
}

export default me;
