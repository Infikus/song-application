import * as actions from '../../actions/drawer.action';
import { LOGOUT } from '../../actions/users.action';

const initialState = {
  open: false,
};

function drawer(state = initialState, action) {
  switch (action.type) {
    case actions.OPEN_SONGS_DRAWER:
      console.log('action OPEN SONGS DRAWER');
      return {
        ...state,
        open: true,
      };
    case actions.CLOSE_SONGS_DRAWER:
      console.log('action CLOSE SONGS DRAWER');
      return {
        ...state,
        open: false,
      };

    case LOGOUT:
      return initialState;

    default:
      return state;
  }
}

export const isSongsDrawerOpen = state => state.open;

export default drawer;
