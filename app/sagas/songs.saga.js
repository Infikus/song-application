import {
  call,
  all,
  takeEvery,
  put,
  select,
} from 'redux-saga/effects';
import * as actions from '../actions/songs.action';
import { getSongsAPI, getSongAPI } from './api/songs.api';
import { handleError401, getAuthHeader } from './tools';
import { getSelectedTeamId } from '../reducers/rootSelectors';

export function* getSong(action) {
  try {
    const { songId } = action.payload;
    const headers = yield call(getAuthHeader);
    const response = yield call(getSongAPI, songId, headers);
    yield put(actions.fetchSongSuccess(response.result, response.entities));
  } catch (e) {
    console.log(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.fetchSongFailure(e));
  }
}

export function* getSongs(action) {
  try {
    const teamId = yield select(getSelectedTeamId);
    const headers = yield call(getAuthHeader);
    const response = yield call(getSongsAPI, teamId, headers);
    yield put(actions.fetchSongsSuccess(response.result, response.entities));
  } catch (e) {
    console.log(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.fetchSongsFailure(e));
  }
}

export function* watcher() {
  yield all([
    takeEvery(actions.FETCH_SONGS_REQUEST, getSongs),
    takeEvery(actions.FETCH_SONG_REQUEST, getSong),
  ]);
}

export default watcher;
