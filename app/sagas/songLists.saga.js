import {
  call,
  all,
  takeEvery,
  put,
  select,
} from 'redux-saga/effects';
import * as actions from '../actions/songLists.action';
import { getSongListsAPI, getSongListAPI } from './api/songLists.api';
import { handleError401, getAuthHeader } from './tools';
import { getSelectedTeamId } from '../reducers/rootSelectors';

export function* getSongList(action) {
  try {
    const headers = yield call(getAuthHeader);
    const response = yield call(getSongListAPI, action.payload.songListId, headers);
    yield put(actions.fetchSongListSuccess(response.result, response.entities));
  } catch (e) {
    console.log(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.fetchSongListFailure(e));
  }
}

export function* getSongLists(action) {
  try {
    const headers = yield call(getAuthHeader);
    const teamId = yield select(getSelectedTeamId);
    const response = yield call(getSongListsAPI, teamId, headers);
    yield put(actions.fetchSongListsSuccess(response.result, response.entities));
  } catch (e) {
    console.log(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.fetchSongListsFailure(e));
  }
}

export function* watcher() {
  yield all([
    takeEvery(actions.FETCH_SONG_LISTS_REQUEST, getSongLists),
    takeEvery(actions.FETCH_SONG_LIST_REQUEST, getSongList),
  ]);
}

export default watcher;
