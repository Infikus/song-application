import fetch from 'cross-fetch';
import { normalize } from 'normalizr';
import constants from './constants';
import { getJSONData } from '../tools';
import { songParamsSchema } from './schemas';

const API_ENDPOINT = `${constants.endpoint}/song-params`;

export function setSongParamsAPI(songParams, headers) {
  const data = {
    ...songParams,
    user: null,
  };
  return fetch(API_ENDPOINT, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  }).then((ret) => {
    if (!ret.ok) return Promise.reject(ret);
    const jsonData = getJSONData(ret);
    return normalize(jsonData, songParamsSchema);
  });
}

export const disableEslint = '';
