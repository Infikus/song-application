import fetch from 'cross-fetch';
import { normalize } from 'normalizr';
import constants from './constants';
import { getJSONData } from '../tools';
import { songsSchema, songSchema } from './schemas';

const API_ENDPOINT = constants.endpoint;

export function getSongAPI(songId, headers) {
  const url = `${API_ENDPOINT}/songs/${songId}`;
  return fetch(url, {
    method: 'GET',
    headers,
  }).then((data) => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, songSchema);
  });
}

export function getSongsAPI(teamId, headers) {
  const url = `${API_ENDPOINT}/songs?teamId=${teamId}`;
  return fetch(url, {
    method: 'GET',
    headers,
  }).then((data) => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    const normData = normalize(jsonData, songsSchema);
    const team = normData.entities.teams ? normData.entities.teams[teamId] : {};
    return {
      ...normData,
      entities: {
        ...normData.entities,
        teams: {
          [teamId]: {
            ...team,
            songs: normData.result,
          },
        },
      },
    };
  });
}
