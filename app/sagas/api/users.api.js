import fetch from 'cross-fetch';
import { normalize } from 'normalizr';
import constants from './constants';
import { getJSONData } from '../tools';
import { userSchema } from './schemas';

const API_ENDPOINT = constants.endpoint;

export function meAPI(headers) {
  return fetch(`${API_ENDPOINT}/me`, {
    method: 'GET',
    headers,
  }).then((data) => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, userSchema);
  });
}

export function refreshTokenAPI(refreshToken) {
  return fetch(`${API_ENDPOINT}/token/refresh`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({
      refresh_token: refreshToken,
    }),
  }).then((data) => {
    if (!data.ok) return Promise.reject(data);
    return getJSONData(data);
  });
}

export function loginAPI(email, password) {
  return fetch(`${API_ENDPOINT}/login_check`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({
      email,
      password,
    }),
  }).then((data) => {
    if (!data.ok) return Promise.reject(data);
    return getJSONData(data);
  });
}

export function registerAPI(email, password) {
  return fetch(`${API_ENDPOINT}/register`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({
      email,
      password,
    }),
  }).then((data) => {
    if (!data.ok) return Promise.reject(data);
    return getJSONData(data);
  });
}
