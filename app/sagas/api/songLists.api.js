import fetch from 'cross-fetch';
import { normalize } from 'normalizr';
import constants from './constants';
import { getJSONData } from '../tools';
import { songListsSchema, songListSchema } from './schemas';

const API_ENDPOINT = `${constants.endpoint}/song-lists`;

export function getSongListAPI(listId, headers) {
  const url = `${API_ENDPOINT}/${listId}`;
  return fetch(url, {
    method: 'GET',
    headers,
  }).then((data) => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, songListSchema);
  });
}

export function getSongListsAPI(teamId, headers) {
  const url = `${API_ENDPOINT}?teamId=${teamId}`;
  return fetch(url, {
    method: 'GET',
    headers,
  }).then((data) => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    const normData = normalize(jsonData, songListsSchema);
    const team = normData.entities.teams ? normData.entities.teams[teamId] : {};
    return {
      ...normData,
      entities: {
        ...normData.entities,
        teams: {
          [teamId]: {
            ...team,
            song_lists: normData.result,
          },
        },
      },
    };
  });
}
