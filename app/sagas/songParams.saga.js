import {
  call,
  takeEvery,
  put,
  all,
} from 'redux-saga/effects';
import * as actions from '../actions/songParams.action';
import { handleError401, getAuthHeader } from './tools';
import { setSongParamsAPI } from './api/songParams.api';

export function* setSongParams(action) {
  try {
    const headers = yield call(getAuthHeader);
    const { songParams } = action.payload;
    const response = yield call(setSongParamsAPI, songParams, headers);
    yield put(actions.setSongParamsSuccess(response.result, response.entities));
  } catch (e) {
    console.error(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.setSongParamsFailure(e));
  }
}

export function* watcher() {
  yield all([
    takeEvery(actions.SET_SONG_PARAMS_REQUEST, setSongParams),
  ]);
}

export default watcher;

