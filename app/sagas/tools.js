import jwtDecode from 'jwt-decode';
import { put, select } from 'redux-saga/effects';
import { logout, refreshTokenRequest, addWaitingAction } from '../actions/users.action';
import { getAccessToken, isTokenRefreshing } from '../reducers/rootSelectors';

export function* getAuthHeader() {
  const token = yield select(getAccessToken);
  return {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${token}`,
  };
}

export function isTokenValid(token) {
  if (!token) return false;
  try {
    const user = jwtDecode(token);
    return user.exp > new Date().getTime() / 1000;
  } catch (e) {
    return false;
  }
}

export function setSelectedTeam(teamId) {
  localStorage.setItem('selectedTeam', teamId);
}

export function* onError() {
  yield put(logout());
}

export function* handleError401(error, action) {
  if (error.status === 401 || error.code === 401) {
    const isRefreshing = yield select(isTokenRefreshing);
    if (!isRefreshing) {
      yield put(refreshTokenRequest());
    }
    yield put(addWaitingAction(action));
    return true;
  }
  return false;
}

export function getJSONData(requestResponse) {
  return JSON.parse(requestResponse._bodyInit); // eslint-disable-line no-underscore-dangle
}

export function getRequestDate(requestResponse) {
  const dateRegex = /([^,]*,[^,]*),.*/;
  return requestResponse.headers.map.date.replace(dateRegex, '$1');
}
