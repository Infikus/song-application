import {
  all,
  fork,
} from 'redux-saga/effects';
import songsSaga from './songs.saga';
import songParamsSaga from './songParams.saga';
import songListsSaga from './songLists.saga';
import usersSaga from './users.saga';

export function* rootSaga(params) {
  yield all([
    fork(songsSaga),
    fork(songListsSaga),
    fork(usersSaga, params),
    fork(songParamsSaga),
  ]);
}

export default rootSaga;
