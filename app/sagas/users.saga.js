import {
  call,
  all,
  put,
  takeLatest,
  takeEvery,
  select,
} from 'redux-saga/effects';
import * as actions from '../actions/users.action';
import { loginAPI, registerAPI, meAPI, refreshTokenAPI } from './api/users.api';
import { onError, getJSONData, getAuthHeader, handleError401 } from './tools';
import { getRefreshToken, getWaitingActions } from '../reducers/rootSelectors';

export function* me(socket, action) {
  try {
    const headers = yield call(getAuthHeader);
    const response = yield call(meAPI, headers);
    yield put(actions.meSuccess(response.result, response.entities));
    yield call(socket.subscribe, response.result);
  } catch (e) {
    console.log(e);
    if (yield call(handleError401, e, action)) return;
    yield call(onError);
  }
}

export function* login(action) {
  try {
    const user = yield call(loginAPI, action.email, action.password);
    yield put(actions.loginSuccess(user));
  } catch (e) {
    console.log(e);
    let msg = 'Identifiants incorrects';
    if (e.status !== 401 && e.code !== 401) {
      msg = 'Une erreur est survenue';
    }
    yield put(actions.loginFailure(msg));
  }
}

export function* register(action) {
  try {
    yield call(registerAPI, action.email, action.password);

    yield put(actions.registerSuccess());
  } catch (e) {
    console.log(e);
    const data = getJSONData(e);
    yield put(actions.registerFailure(data.error));
  }
}

export function* refreshToken() {
  try {
    console.log('refreshToken');
    const token = yield select(getRefreshToken);
    const data = yield call(refreshTokenAPI, token);
    const waitingActions = yield select(getWaitingActions);
    console.log('waiting actions', waitingActions.length);
    yield put(actions.refreshTokenSuccess(data.token));
    for (let i = 0; i < waitingActions.length; i += 1) {
      yield put(waitingActions[i]);
    }
  } catch (e) {
    console.log(e);
    yield call(onError);
  }
}

export function* watcher(params) {
  yield all([
    takeLatest(actions.LOGIN_REQUEST, login),
    takeEvery(actions.REGISTER_REQUEST, register),
    takeLatest(actions.ME_REQUEST, me, params.socket),
    takeLatest(actions.REFRESH_TOKEN_REQUEST, refreshToken),
  ]);
}

export default watcher;
