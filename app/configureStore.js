import { AsyncStorage } from 'react-native';
import devTools from 'remote-redux-devtools';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import rootReducer from './reducers/root.reducer';
import saga from './sagas/app.saga';
import configureSocket from './socket';
import authMiddleware from './middlewares/auth.middleware';

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();

  const enhancer = compose(
    applyMiddleware(authMiddleware, sagaMiddleware),
    devTools({
      name: 'songApp', realtime: true,
    }),
  );

  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
  };

  const persistedReducer = persistReducer(persistConfig, rootReducer);

  const store = createStore(persistedReducer, enhancer);
  const persistor = persistStore(store);

  const socket = configureSocket(store.dispatch);

  sagaMiddleware.run(saga, { socket });

  return { store, persistor };
}
