import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Text } from 'native-base';
import { LogInNavigator as LogIn, RootStackNavigator as Main } from '../router';
import { getAccessToken, isPending, isSuccessful, isSongsDrawerOpen } from '../reducers/rootSelectors';
import { fetchSongListsRequest } from '../actions/songLists.action';
import { meRequest } from '../actions/users.action';
import { fetchSongsRequest } from '../actions/songs.action';
import { closeSongsDrawer, openSongsDrawer } from '../actions/drawer.action';


class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: !!props.accessToken,
      initialLoad: true,
    };
    this.onNavigationStateChange = this.onNavigationStateChange.bind(this);
  }

  componentDidMount() {
    if (this.state.loggedIn) {
      this.props.getMe();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.accessToken && !this.props.accessToken) {
      this.setState({
        loggedIn: true,
      });
      nextProps.getMe();
    } else if (this.props.accessToken && !nextProps.accessToken) {
      // If disconnected for instance
      this.setState({
        initialLoad: true,
        loggedIn: false,
      });
    } else if (nextProps.meSuccess && !this.props.meSuccess) {
      nextProps.getSongLists();
      nextProps.getSongs();
    } else if (!this.isPending(nextProps) && this.isPending(this.props)) {
      this.setState({
        initialLoad: false,
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.loggedIn !== this.state.loggedIn) {
      return true;
    } else if (this.state.initialLoad !== nextState.initialLoad) {
      return true;
    }

    return false;
  }

  onNavigationStateChange(previous, next, action) {
    if (action.type === 'Navigation/NAVIGATE') {
      if (action.routeName === 'DrawerClose' && this.props.isDrawerOpen) {
        this.props.closeDrawer();
      } else if (action.routeName === 'DrawerOpen' && !this.props.isDrawerOpen) {
        this.props.openDrawer();
      }
    }
  }

  isPending(props = this.props) {
    return props.songsPending || props.songListsPending || props.mePending;
  }

  render() {
    if (!this.state.loggedIn) {
      return <LogIn />;
    } else if (this.state.initialLoad) {
      return <Text>Pending...</Text>;
    }
    return (
      <Main onNavigationStateChange={this.onNavigationStateChange} />
    );
  }
}

SplashScreen.propTypes = {
  accessToken: PropTypes.string,
  getSongLists: PropTypes.func.isRequired,
  getSongs: PropTypes.func.isRequired,
  getMe: PropTypes.func.isRequired,
  songListsPending: PropTypes.bool.isRequired, // eslint-disable-line react/no-unused-prop-types
  mePending: PropTypes.bool.isRequired, // eslint-disable-line react/no-unused-prop-types
  songsPending: PropTypes.bool.isRequired, // eslint-disable-line react/no-unused-prop-types
  meSuccess: PropTypes.bool.isRequired,
  closeDrawer: PropTypes.func.isRequired,
  openDrawer: PropTypes.func.isRequired,
  isDrawerOpen: PropTypes.bool.isRequired,
};

SplashScreen.defaultProps = {
  accessToken: undefined,
};

const mapStateToProps = state => ({
  accessToken: getAccessToken(state),
  songListsPending: isPending('FETCH_SONG_LISTS', state),
  songsPending: isPending('FETCH_SONGS', state),
  mePending: isPending('ME', state),
  meSuccess: isSuccessful('ME', state),
  isDrawerOpen: isSongsDrawerOpen(state),
});

const mapDispatchToProps = dispatch => ({
  getSongLists: () => dispatch(fetchSongListsRequest()),
  getSongs: () => dispatch(fetchSongsRequest()),
  getMe: () => dispatch(meRequest()),
  closeDrawer: () => dispatch(closeSongsDrawer()),
  openDrawer: () => dispatch(openSongsDrawer()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
