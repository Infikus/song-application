import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Content, Header, Left, Button, Icon, Title, Body } from 'native-base';
import SongListItemsComponent from '../components/SongsDrawer/SongListItems.component';
import { setSelectedSong } from '../actions/users.action';
import { getSongListItemsOfSongList, isPending } from '../reducers/rootSelectors';
import { closeSongsDrawer } from '../actions/drawer.action';

class SongListSongsDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: undefined,
    };

    this.onSelect = this.onSelect.bind(this);
    this.onBackPressed = this.onBackPressed.bind(this);
  }

  onSelect(songListItem) {
    const { songListId } = this.props.navigation.state.params;
    this.props.setSelectedSong(songListItem.id, songListId);
    this.props.closeDrawer();
  }

  onBackPressed() {
    this.props.navigation.goBack();
  }

  render() {
    const { songListItems } = this.props;
    const { selectedItem } = this.state;
    return (
      <Container>
        <Header>
          <Left>
            <Button onPress={this.onBackPressed} transparent>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>SongList.Name</Title>
          </Body>
        </Header>
        <Content>
          <SongListItemsComponent
            songListItems={songListItems}
            selectedItem={selectedItem}
            onSelect={this.onSelect}
          />
        </Content>
      </Container>
    );
  }
}

SongListSongsDrawer.propTypes = {
  songListItems: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    position: PropTypes.number.isRequired,
    song: PropTypes.shape({
      id: PropTypes.number.isRequired,
      content: PropTypes.string.isRequired,
    }).isRequired,
  })).isRequired,
  setSelectedSong: PropTypes.func.isRequired,
  navigation: PropTypes.object, // eslint-disable-line
  closeDrawer: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  songListItems: getSongListItemsOfSongList(ownProps.navigation.state.params ?
    ownProps.navigation.state.params.songListId :
    undefined, state),
  pending: isPending('FETCH_SONG_LISTS', state),
});

const mapDispatchToProps = dispatch => ({
  setSelectedSong: (songId, songListId) => dispatch(setSelectedSong(songId, songListId)),
  closeDrawer: () => dispatch(closeSongsDrawer()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongListSongsDrawer);

