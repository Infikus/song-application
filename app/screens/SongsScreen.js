import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container, Header, Left, Body, Title, Button, Icon, Right, Footer, Text } from 'native-base';
import { setSelectedSong, setSelectedSongDone } from '../actions/users.action';
import { getSelectedSongId, getSelectedSongListId, isSongSelected, getSongListItemsOfSongList, isSongsDrawerOpen, isPending } from '../reducers/rootSelectors';
import { getSelectedTeamSongs } from '../reducers/selectors/songs.selector';
import Songs from '../components/Songs/Songs.component';
import SongListItems from '../components/Songs/SongListItems.component';
import { areSameSongListItemsArray } from './utils';
import CapoModal from '../components/Modals/CapoModal.component';
import { setSongParamsRequest } from '../actions/songParams.action';


class SongsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      capoModal: false,
    };

    this.onIndexChanged = this.onIndexChanged.bind(this);
    this.onIndexChangedSongListItems = this.onIndexChangedSongListItems.bind(this);
    this.onIndexChangedSongs = this.onIndexChangedSongs.bind(this);
    this.toggleCapoModal = this.toggleCapoModal.bind(this);
    this.onCapoChange = this.onCapoChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSongSelected) {
      this.props.resetIsSongSelected();
    }
    if (nextProps.isDrawerOpen && !this.props.isDrawerOpen) {
      this.props.navigation.navigate('DrawerOpen');
    } else if (!nextProps.isDrawerOpen && this.props.isDrawerOpen) {
      this.props.navigation.navigate('DrawerClose');
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state !== nextState) {
      return true;
    }

    if (nextProps.selectedSongId !== this.props.selectedSongId) {
      return true;
    } else if (nextProps.selectedSongListId !== undefined) {
      return !areSameSongListItemsArray(nextProps.songListItems, this.props.songListItems);
    }
    // const differentSongs = !areSameSongArray(nextProps.songs, this.props.songs);
    const differentSongs = nextProps.songs !== this.props.songs;
    if (differentSongs) {
      return true;
    } else if (nextProps.selectedSongListId === undefined &&
      this.props.selectedSongListId !== undefined) {
      return true;
    }

    return true;
  }

  onIndexChangedSongs(index) {
    const selectedSongId = this.props.songs[index].id;
    if (this.props.selectedSongId !== selectedSongId) {
      this.props.setSelectedSong(selectedSongId);
    }
  }

  onIndexChangedSongListItems(index) {
    // If the songListItems in props are not right, we need to wait
    if (this.props.songListItems.length === 0 ||
      this.props.songListItems[0].song_list !== this.props.selectedSongListId) {
      return;
    }
    // Else we can update the selectedSong
    const songListItem = this.props.songListItems.find(item => item.position === index);
    const selectedItemId = songListItem.id;
    if (this.props.selectedSongId !== selectedItemId) {
      this.props.setSelectedSong(songListItem.id, this.props.selectedSongListId);
    }
  }

  onIndexChanged(index) {
    if (this.props.selectedSongListId === undefined) {
      this.onIndexChangedSongs(index);
    } else {
      this.onIndexChangedSongListItems(index);
    }
  }

  onCapoChange(capo) {
    const defaultSongParams = {
      capo: 0,
      transpose: 0,
    };
    const { selectedSongListId, selectedSongId } = this.props;
    // Get the songParams
    let songParams = null;
    if (selectedSongListId !== undefined) {
      const item = this.props.songListItems.find(e => e.id === selectedSongId);
      songParams = item.song_params && item.song_params.length > 0 ?
        item.song_params[0] :
        defaultSongParams;
      songParams.song_list_item = {
        id: item.id,
      };
    } else {
      const song = this.props.songs.find(s => s.id === selectedSongId);
      songParams = song.song_params && song.song_params.length > 0 ?
        song.song_params[0] :
        defaultSongParams;
      songParams.song = {
        id: song.id,
      };
    }
    songParams.capo = capo;

    this.props.updateSongParams(songParams);

    // Send this new songParams to server
    this.setState({
      capoModal: false,
    });
  }

  getTitle() {
    const { songsPending, selectedSongId, selectedSongListId } = this.props;

    if (songsPending && selectedSongId !== undefined) {
      return 'Loading...';
    } else if (selectedSongId === undefined) {
      return 'Rien à afficher';
    } else if (selectedSongListId !== undefined) {
      const item = this.props.songListItems.find(e => e.id === selectedSongId);
      return item.song.title;
    }

    const song = this.props.songs.find(e => e.id === selectedSongId);
    return song.title;
  }

  getCurrentKey() {
    if (this.props.selectedSongListId === undefined) {
      const song = this.props.songs.find(s => s.id === this.props.selectedSongId);
      return song ? song.key : 0;
    }
    return 0;
  }

  toggleCapoModal() {
    this.setState(prevState => ({
      capoModal: !prevState.capoModal,
    }));
  }

  areSongListItemsValid() {
    const { songListItems, selectedSongListId } = this.props;
    return songListItems.length > 0 &&
      songListItems[0].song_list === selectedSongListId;
  }

  renderSongs() {
    if (this.props.selectedSongListId === undefined) {
      // Show all songs
      const { songs, selectedSongId } = this.props;
      if (songs.length > 0 && selectedSongId !== undefined) {
        return (
          <Songs
            ref={(ref) => { this.songsComponent = ref; }}
            songs={songs}
            onIndexChanged={this.onIndexChanged}
            selectedSongId={selectedSongId}
          />
        );
      }
      return null;
    }
    // Show the songs of the selected song list
    if (!this.areSongListItemsValid()) {
      return null;
    }
    const { songListItems, selectedSongId, selectedSongListId } = this.props;
    // The key is use to force the remounting of the component when the songListId change
    return (
      <SongListItems
        key={selectedSongListId}
        ref={(ref) => { this.songsListItemsComponent = ref; }}
        songListItems={songListItems}
        selectedSongId={selectedSongId}
        onIndexChanged={this.onIndexChanged}
      />
    );
  }

  render() {
    const { navigation } = this.props;
    const { capoModal } = this.state;
    const currentKey = this.getCurrentKey();
    return (
      <Container>
        <Header>
          <Left>
            <Button onPress={() => navigation.navigate('DrawerOpen')} transparent>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>{ this.getTitle() }</Title>
          </Body>
          <Right>
            <Button onPress={() => navigation.navigate('SettingsModal')} transparent>
              <Icon name="settings" />
            </Button>
          </Right>
        </Header>
        { this.renderSongs() }
        <Footer>
          <Right>
            <Button
              transparent
              light
              onPress={this.toggleCapoModal}
            >
              <Text>Capo</Text>
            </Button>
          </Right>
        </Footer>
        <CapoModal
          capo={0}
          songKey={currentKey}
          visible={capoModal}
          onClose={this.toggleCapoModal}
          onCapoChange={this.onCapoChange}
        />
      </Container>
    );
  }
}

SongsScreen.propTypes = {
  songs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    content: PropTypes.string.isRequired,
  })).isRequired,
  songListItems: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    song: PropTypes.shape({
      id: PropTypes.number.isRequired,
      content: PropTypes.string.isRequired,
    }).isRequired,
    song_list: PropTypes.number.isRequired,
  })).isRequired,
  selectedSongId: PropTypes.number,
  selectedSongListId: PropTypes.number,
  isSongSelected: PropTypes.bool.isRequired,
  setSelectedSong: PropTypes.func.isRequired,
  resetIsSongSelected: PropTypes.func.isRequired,
  navigation: PropTypes.object, // eslint-disable-line
  isDrawerOpen: PropTypes.bool.isRequired,
  songsPending: PropTypes.bool.isRequired,
  updateSongParams: PropTypes.func.isRequired,
};

SongsScreen.defaultProps = {
  selectedSongId: undefined,
  selectedSongListId: undefined,
};

const mapStateToProps = (state) => {
  const selectedSongListId = getSelectedSongListId(state);
  return {
    songs: getSelectedTeamSongs(state),
    songListItems: getSongListItemsOfSongList(selectedSongListId, state),
    selectedSongId: getSelectedSongId(state),
    isSongSelected: isSongSelected(state),
    isDrawerOpen: isSongsDrawerOpen(state),
    songsPending: isPending('FETCH_SONGS', state),
    selectedSongListId,
  };
};

const mapDispatchToProps = dispatch => ({
  setSelectedSong: (songId, songListId) => dispatch(setSelectedSong(songId, songListId)),
  updateSongParams: songParams => dispatch(setSongParamsRequest(songParams)),
  resetIsSongSelected: () => dispatch(setSelectedSongDone()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongsScreen);
