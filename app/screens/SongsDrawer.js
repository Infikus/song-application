import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Text, View } from 'native-base';
import SongsComponent from '../components/SongsDrawer/Songs.component';
import { isPending, isSongsDrawerOpen } from '../reducers/rootSelectors';
import { getSelectedTeamSongs } from '../reducers/selectors/songs.selector';
import { fetchSongsRequest } from '../actions/songs.action';
import { setSelectedSong } from '../actions/users.action';
import { closeSongsDrawer } from '../actions/drawer.action';

class SongsDrawer extends Component {
  static navigationOptions = ({
    title: 'Chants',
  });

  constructor(props) {
    super(props);
    this.state = {
      selectedSong: undefined,
    };

    this.onSongSelect = this.onSongSelect.bind(this);
  }

  componentDidMount() {
    this.props.getSongs();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen && !this.props.isOpen) {
      nextProps.getSongs();
    }
  }

  shouldComponentUpdate(nextProps) {
    if (this.props.pending !== nextProps.pending) {
      return true;
    }
    return nextProps.songs !== this.props.songs;
  }

  onSongSelect(song) {
    this.props.setSelectedSong(song.id);
    this.props.closeDrawer();
  }

  render() {
    const { songs, pending } = this.props;
    const { selectedSong } = this.state;
    return (
      <View style={{ flex: 1 }}>
        { pending && <Text>Loading...</Text>}
        <SongsComponent
          songs={songs}
          selectedSong={selectedSong}
          onSelect={this.onSongSelect}
        />
      </View>
    );
  }
}

SongsDrawer.propTypes = {
  songs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
  })).isRequired,
  getSongs: PropTypes.func.isRequired,
  setSelectedSong: PropTypes.func.isRequired,
  navigation: PropTypes.object, // eslint-disable-line
  closeDrawer: PropTypes.func.isRequired,
  pending: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  songs: getSelectedTeamSongs(state),
  pending: isPending('FETCH_SONGS', state),
  isOpen: isSongsDrawerOpen(state),
});

const mapDispatchToProps = dispatch => ({
  getSongs: () => dispatch(fetchSongsRequest()),
  setSelectedSong: songId => dispatch(setSelectedSong(songId)),
  closeDrawer: () => dispatch(closeSongsDrawer()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongsDrawer);
