function compareSongs(s1, s2) {
  return s1.id === s2.id &&
    s1.content === s2.content &&
    s1.title === s2.title &&
    s1.key === s2.key &&
    s1.author === s2.author;
}

function compareSongLists(l1, l2) {
  return l1.id === l2.id &&
    l1.name === l2.name;
}

function compareSongParams(s1, s2) {
  return (s1 === null && s2 === null) ||
    (s1 && s2 && s1[0].id === s2[0].id &&
      s1[0].capo === s2[0].capo && s1[0].transpose === s2[0].transpose);
}

function compareSongListItems(i1, i2) {
  return i1.id === i2.id &&
    i1.position === i2.position &&
    compareSongs(i1.song, i2.song) &&
    compareSongParams(i1.song_params, i2.song_params);
}

export function areSameSongArray(arr1, arr2) {
  if (arr1.length !== arr2.length) {
    return false;
  }
  for (let i = 0; i < arr1.length; i += 1) {
    if (!compareSongs(arr1[i], arr2[i])) {
      return false;
    }
  }
  return true;
}

export function areSameSongListItemsArray(arr1, arr2) {
  if (arr1 === undefined && arr2 === undefined) return true;
  if (arr1 === undefined || arr2 === undefined) return false;
  if (arr1.length !== arr2.length) {
    return false;
  }
  for (let i = 0; i < arr1.length; i += 1) {
    if (!compareSongListItems(arr1[i], arr2[i])) {
      return false;
    }
  }
  return true;
}

export function areSameSongListArray(arr1, arr2) {
  if (arr1.length !== arr2.length) {
    return false;
  }
  for (let i = 0; i < arr1.length; i += 1) {
    if (!compareSongLists(arr1[i], arr2[i])) {
      console.log(arr1[i]);
      return false;
    }
  }
  return true;
}
