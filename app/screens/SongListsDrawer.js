import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Text, Content } from 'native-base';
import { getSelectedTeamSongLists, isSongsDrawerOpen, isPending } from '../reducers/rootSelectors';
import { fetchSongListsRequest } from '../actions/songLists.action';
import SongListsComponent from '../components/SongsDrawer/SongLists.component';
import { areSameSongListArray } from './utils';

class SongListsDrawer extends Component {
  static navigationOptions = ({
    title: 'Listes',
  });

  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
  }

  componentDidMount() {
    this.props.getSongLists();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen && !this.props.isOpen) {
      nextProps.getSongLists();
    }
  }

  shouldComponentUpdate(nextProps) {
    if (this.props.pending !== nextProps.pending) {
      return true;
    }
    return !areSameSongListArray(nextProps.songLists, this.props.songLists);
  }

  onSelect(songList) {
    this.props.navigation.navigate('SongListSongs', {
      songListId: songList.id,
    });
  }

  render() {
    const { songLists, pending } = this.props;
    return (
      <Content>
        { pending && <Text>Loading...</Text>}
        <SongListsComponent
          songLists={songLists}
          onSelect={this.onSelect}
        />
      </Content>
    );
  }
}

SongListsDrawer.propTypes = {
  songLists: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
  })).isRequired,
  getSongLists: PropTypes.func.isRequired,
  navigation: PropTypes.object, // eslint-disable-line
  isOpen: PropTypes.bool.isRequired,
  pending: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  songLists: getSelectedTeamSongLists(state),
  pending: isPending('FETCH_SONG_LISTS', state),
  isOpen: isSongsDrawerOpen(state),
});

const mapDispatchToProps = dispatch => ({
  getSongLists: () => dispatch(fetchSongListsRequest()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongListsDrawer);
