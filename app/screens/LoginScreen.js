import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Content } from 'native-base';

import LoginFormComponent from '../components/Login/LoginForm.component';
import { isPending } from '../reducers/rootSelectors';
import { loginRequest } from '../actions/users.action';

const LoginScreen = ({
  login,
}) => (
  <Container>
    <Content padder>
      <LoginFormComponent onSubmit={(values) => {
          login(values);
        }}
      />
    </Content>
  </Container>
);

LoginScreen.propTypes = {
  login: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  pending: isPending('LOGIN', state),
});

const mapDispatchToProps = dispatch => ({
  login: data => dispatch(loginRequest(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
