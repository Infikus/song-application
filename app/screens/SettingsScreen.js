import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Content, Header, Body, Left, Button, Icon, List, Title } from 'native-base';
import GroupPickerComponent from '../components/Settings/GroupPicker.component';
import { getMyTeams, getSelectedTeamId } from '../reducers/rootSelectors';
import { setSelectedTeam as setSelectedTeamAction, logout as logoutAction } from '../actions/users.action';
import { fetchSongsRequest } from '../actions/songs.action';
import { fetchSongListsRequest } from '../actions/songLists.action';
import LogoutItem from '../components/Settings/LogoutItem.component';

class SettingsScreen extends Component {
  constructor(props) {
    super(props);

    this.goBack = this.goBack.bind(this);
    this.onSetSelectedTeam = this.onSetSelectedTeam.bind(this);
  }

  onSetSelectedTeam(teamId) {
    if (teamId !== this.props.selectedTeamId) {
      // If the selected team has changed, load songs and songLists
      this.props.setSelectedTeam(teamId);
      this.props.getSongLists();
      this.props.getSongs();
    }
  }

  goBack() {
    this.props.navigation.goBack();
  }

  render() {
    const { teams, selectedTeamId, logout } = this.props;
    return (
      <Container>
        <Header>
          <Left>
            <Button onPress={this.goBack} transparent>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Paramètres</Title>
          </Body>
        </Header>
        <Content>
          <List>
            <GroupPickerComponent
              teams={teams}
              current={selectedTeamId}
              onTeamSelect={this.onSetSelectedTeam}
            />
            <LogoutItem
              onPress={logout}
            />
          </List>
        </Content>
      </Container>
    );
  }
}

SettingsScreen.propTypes = {
  navigation: PropTypes.object.isRequired, // eslint-disable-line
  teams: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  setSelectedTeam: PropTypes.func.isRequired,
  selectedTeamId: PropTypes.number.isRequired,
  getSongs: PropTypes.func.isRequired,
  getSongLists: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
};

SettingsScreen.defaultProps = {};

const mapStateToProps = state => ({
  teams: getMyTeams(state),
  selectedTeamId: getSelectedTeamId(state),
});

const mapDispatchToProps = dispatch => ({
  setSelectedTeam: teamId => dispatch(setSelectedTeamAction(teamId)),
  getSongs: () => dispatch(fetchSongsRequest()),
  getSongLists: () => dispatch(fetchSongListsRequest()),
  logout: () => dispatch(logoutAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);
