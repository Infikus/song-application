import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import SplashScreen from './screens/SplashScreen';
import configureStore from './configureStore';

class Root extends Component {
  constructor(props) {
    super(props);
    const { store, persistor } = configureStore();
    this.state = {
      store,
      persistor,
    };
  }

  render() {
    const { store, persistor } = this.state;
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <SplashScreen />
        </PersistGate>
      </Provider>
    );
  }
}

export default Root;
