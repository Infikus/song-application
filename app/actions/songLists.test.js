import * as actions from './songLists.action';

describe('songLists actions', () => {
  it('test fetchSongListsRequest', () => {
    const expectedAction = {
      type: actions.FETCH_SONG_LISTS_REQUEST,
      payload: {},
    };

    expect(actions.fetchSongListsRequest()).toEqual(expectedAction);
  });
  it('test fetchSongListsSuccess', () => {
    const songListsId = [1, 2];
    const entities = {
      1: {},
      2: {},
    };
    const expectedAction = {
      type: actions.FETCH_SONG_LISTS_SUCCESS,
      payload: {
        songListsId,
        entities,
      },
    };

    expect(actions.fetchSongListsSuccess(songListsId, entities)).toEqual(expectedAction);
  });
  it('test fetchSongListsFailure', () => {
    const error = 'error';
    const expectedAction = {
      type: actions.FETCH_SONG_LISTS_FAILURE,
      payload: {
        error,
      },
    };

    expect(actions.fetchSongListsFailure(error)).toEqual(expectedAction);
  });

  it('test fetchSongListRequest', () => {
    const songListId = 1;
    const expectedAction = {
      type: actions.FETCH_SONG_LIST_REQUEST,
      payload: {
        songListId,
      },
    };

    expect(actions.fetchSongListRequest(songListId)).toEqual(expectedAction);
  });
  it('test fetchSongListSuccess', () => {
    const songListId = 1;
    const entities = {
      1: {},
    };
    const expectedAction = {
      type: actions.FETCH_SONG_LIST_SUCCESS,
      payload: {
        songListId,
        entities,
      },
    };

    expect(actions.fetchSongListSuccess(songListId, entities)).toEqual(expectedAction);
  });
  it('test fetchSongListFailure', () => {
    const error = 'error';
    const expectedAction = {
      type: actions.FETCH_SONG_LIST_FAILURE,
      payload: {
        error,
      },
    };

    expect(actions.fetchSongListFailure(error)).toEqual(expectedAction);
  });
});
