export const OPEN_SONGS_DRAWER = 'OPEN_SONGS_DRAWER';

export const openSongsDrawer = () => ({
  type: OPEN_SONGS_DRAWER,
  payload: {},
});

export const CLOSE_SONGS_DRAWER = 'CLOSE_SONGS_DRAWER';

export const closeSongsDrawer = () => ({
  type: CLOSE_SONGS_DRAWER,
  payload: {},
});
