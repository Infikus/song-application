import * as actions from './songs.action';

describe('songs actions', () => {
  it('test fetchSongsRequest', () => {
    const expectedAction = {
      type: actions.FETCH_SONGS_REQUEST,
      payload: {},
    };

    expect(actions.fetchSongsRequest()).toEqual(expectedAction);
  });
  it('test fetchSongsSuccess', () => {
    const songsId = [1, 2];
    const entities = {
      1: {},
      2: {},
    };
    const expectedAction = {
      type: actions.FETCH_SONGS_SUCCESS,
      payload: {
        songsId,
        entities,
      },
    };

    expect(actions.fetchSongsSuccess(songsId, entities)).toEqual(expectedAction);
  });
  it('test fetchSongsFailure', () => {
    const error = 'error';
    const expectedAction = {
      type: actions.FETCH_SONGS_FAILURE,
      payload: {
        error,
      },
    };

    expect(actions.fetchSongsFailure(error)).toEqual(expectedAction);
  });

  it('test fetchSongRequest', () => {
    const songId = 1;
    const expectedAction = {
      type: actions.FETCH_SONG_REQUEST,
      payload: {
        songId,
      },
    };

    expect(actions.fetchSongRequest(songId)).toEqual(expectedAction);
  });
  it('test fetchSongSuccess', () => {
    const songId = 1;
    const entities = {
      1: {},
    };
    const expectedAction = {
      type: actions.FETCH_SONG_SUCCESS,
      payload: {
        songId,
        entities,
      },
    };

    expect(actions.fetchSongSuccess(songId, entities)).toEqual(expectedAction);
  });
  it('test fetchSongFailure', () => {
    const error = 'error';
    const expectedAction = {
      type: actions.FETCH_SONG_FAILURE,
      payload: {
        error,
      },
    };

    expect(actions.fetchSongFailure(error)).toEqual(expectedAction);
  });
});
