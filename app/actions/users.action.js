export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

/* Action creators */
export const loginRequest = ({ email, password }) => ({
  type: LOGIN_REQUEST,
  email,
  password,
});
export const loginSuccess = user => ({
  type: LOGIN_SUCCESS,
  user,
});
export const loginFailure = error => ({
  type: LOGIN_FAILURE,
  payload: {
    error,
  },
});

export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';
export const REGISTER_RESET = 'REGISTER_RESET';

/* Action creators */
export const registerRequest = ({ email, password }) => ({
  type: REGISTER_REQUEST,
  email,
  password,
});
export const registerSuccess = () => ({
  type: REGISTER_SUCCESS,
});
export const registerFailure = error => ({
  type: REGISTER_FAILURE,
  payload: {
    error,
  },
});

export const ME_REQUEST = 'ME_REQUEST';
export const ME_SUCCESS = 'ME_SUCCESS';
export const ME_FAILURE = 'ME_FAILURE';

export const meRequest = () => ({
  type: ME_REQUEST,
  payload: {},
});
export const meSuccess = (userId, entities) => ({
  type: ME_SUCCESS,
  payload: {
    userId,
    entities,
  },
});
export const meFailure = error => ({
  type: ME_FAILURE,
  payload: {
    error,
  },
});

export const SET_SELECTED_TEAM = 'SET_SELECTED_TEAM';

export const setSelectedTeam = teamId => ({
  type: SET_SELECTED_TEAM,
  payload: {
    teamId,
  },
});

export const SET_SELECTED_SONG = 'SET_SELECTED_SONG';

export const setSelectedSong = (songId, songListId) => ({
  type: SET_SELECTED_SONG,
  payload: {
    songId,
    songListId,
  },
});

export const SET_SELECTED_SONG_DONE = 'SET_SELECTED_SONG_DONE';

export const setSelectedSongDone = () => ({
  type: SET_SELECTED_SONG_DONE,
  payload: {},
});

export const LOGOUT = 'LOGOUT';

export const logout = () => ({
  type: LOGOUT,
  payload: {},
});

export const REFRESH_TOKEN_REQUEST = 'REFRESH_TOKEN_REQUEST';

export const refreshTokenRequest = () => ({
  type: REFRESH_TOKEN_REQUEST,
  payload: {},
});

export const ADD_WAITING_ACTION = 'ADD_WAITING_ACTION';

export const addWaitingAction = action => ({
  type: ADD_WAITING_ACTION,
  payload: {
    action,
  },
});

export const REFRESH_TOKEN_SUCCESS = 'REFRESH_TOKEN_SUCCESS';

export const refreshTokenSuccess = token => ({
  type: REFRESH_TOKEN_SUCCESS,
  payload: {
    token,
  },
});
