export const FETCH_SONG_LISTS_REQUEST = 'FETCH_SONG_LISTS_REQUEST';
export const FETCH_SONG_LISTS_SUCCESS = 'FETCH_SONG_LISTS_SUCCESS';
export const FETCH_SONG_LISTS_FAILURE = 'FETCH_SONG_LISTS_FAILURE';

export const fetchSongListsRequest = () => ({
  type: FETCH_SONG_LISTS_REQUEST,
  payload: {},
});
export const fetchSongListsSuccess = (songListsId, entities) => ({
  type: FETCH_SONG_LISTS_SUCCESS,
  payload: {
    songListsId,
    entities,
  },
});
export const fetchSongListsFailure = error => ({
  type: FETCH_SONG_LISTS_FAILURE,
  payload: {
    error,
  },
});

export const FETCH_SONG_LIST_REQUEST = 'FETCH_SONG_LIST_REQUEST';
export const FETCH_SONG_LIST_SUCCESS = 'FETCH_SONG_LIST_SUCCESS';
export const FETCH_SONG_LIST_FAILURE = 'FETCH_SONG_LIST_FAILURE';

export const fetchSongListRequest = songListId => ({
  type: FETCH_SONG_LIST_REQUEST,
  payload: {
    songListId,
  },
});
export const fetchSongListSuccess = (songListId, entities) => ({
  type: FETCH_SONG_LIST_SUCCESS,
  payload: {
    songListId,
    entities,
  },
});
export const fetchSongListFailure = error => ({
  type: FETCH_SONG_LIST_FAILURE,
  payload: {
    error,
  },
});
