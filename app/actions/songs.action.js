export const FETCH_SONGS_REQUEST = 'FETCH_SONGS_REQUEST';
export const FETCH_SONGS_SUCCESS = 'FETCH_SONGS_SUCCESS';
export const FETCH_SONGS_FAILURE = 'FETCH_SONGS_FAILURE';

export const fetchSongsRequest = () => ({
  type: FETCH_SONGS_REQUEST,
  payload: {},
});
export const fetchSongsSuccess = (songsId, entities) => ({
  type: FETCH_SONGS_SUCCESS,
  payload: {
    songsId,
    entities,
  },
});
export const fetchSongsFailure = error => ({
  type: FETCH_SONGS_FAILURE,
  payload: {
    error,
  },
});

export const FETCH_SONG_REQUEST = 'FETCH_SONG_REQUEST';
export const FETCH_SONG_SUCCESS = 'FETCH_SONG_SUCCESS';
export const FETCH_SONG_FAILURE = 'FETCH_SONG_FAILURE';

export const fetchSongRequest = songId => ({
  type: FETCH_SONG_REQUEST,
  payload: {
    songId,
  },
});
export const fetchSongSuccess = (songId, entities) => ({
  type: FETCH_SONG_SUCCESS,
  payload: {
    songId,
    entities,
  },
});
export const fetchSongFailure = error => ({
  type: FETCH_SONG_FAILURE,
  payload: {
    error,
  },
});
