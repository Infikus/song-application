import * as actions from './users.action';

describe('users actions', () => {
  it('test meRequest', () => {
    const expectedAction = {
      type: actions.ME_REQUEST,
      payload: {},
    };

    expect(actions.meRequest()).toEqual(expectedAction);
  });
  it('test meSuccess', () => {
    const userId = 2;
    const entities = {
      1: {},
      2: {},
    };
    const expectedAction = {
      type: actions.ME_SUCCESS,
      payload: {
        userId,
        entities,
      },
    };

    expect(actions.meSuccess(userId, entities)).toEqual(expectedAction);
  });
  it('test meFailure', () => {
    const error = 'error';
    const expectedAction = {
      type: actions.ME_FAILURE,
      payload: {
        error,
      },
    };

    expect(actions.meFailure(error)).toEqual(expectedAction);
  });

  it('test setSelectedTeam', () => {
    const teamId = 5;
    const expectedAction = {
      type: actions.SET_SELECTED_TEAM,
      payload: {
        teamId,
      },
    };

    expect(actions.setSelectedTeam(teamId)).toEqual(expectedAction);
  });

  it('test setSelectedSong', () => {
    const songId = 5;
    const songListId = 7;
    const expectedAction = {
      type: actions.SET_SELECTED_SONG,
      payload: {
        songId,
        songListId,
      },
    };

    expect(actions.setSelectedSong(songId, songListId)).toEqual(expectedAction);
  });
});
