import { getAccessToken } from '../reducers/rootSelectors';
import { REFRESH_TOKEN_REQUEST, REFRESH_TOKEN_SUCCESS, ADD_WAITING_ACTION, addWaitingAction } from '../actions/users.action';
import { isTokenRefreshing } from '../reducers/app/me.reducer';
import { OPEN_SONGS_DRAWER, CLOSE_SONGS_DRAWER } from '../actions/drawer.action';

// List of actions that should not add in waitingActions array
const authorizedActions = [
  REFRESH_TOKEN_REQUEST,
  REFRESH_TOKEN_SUCCESS,
  ADD_WAITING_ACTION,
  OPEN_SONGS_DRAWER,
  CLOSE_SONGS_DRAWER,
];

const shouldIgnoreAction = action =>
  authorizedActions.indexOf(action.type) >= 0 ||
  action.type.includes('FAILURE') ||
  action.type.includes('SUCCESS');

export default function authMiddleware({ dispatch, getState }) {
  return next => (action) => {
    const state = getState();
    const token = getAccessToken(state);
    if (!shouldIgnoreAction(action) && token) {
      // make sure we are not already refreshing the token
      const isRefreshing = isTokenRefreshing(state);
      if (isRefreshing) {
        dispatch(addWaitingAction(action));
        return;
      }
    }

    return next(action); // eslint-disable-line consistent-return
  };
}
