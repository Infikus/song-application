import React from 'react';
import { StackNavigator, DrawerNavigator, TabNavigator } from 'react-navigation';
import Login from './screens/LoginScreen';
import SongsDrawer from './screens/SongsDrawer';
import SongListsDrawer from './screens/SongListsDrawer';

import SongsScreen from './screens/SongsScreen';
import SongListSongsDrawer from './screens/SongListSongsDrawer';
import SettingsScreen from './screens/SettingsScreen';

export const LogInNavigator = StackNavigator(
  {
    LogIn: {
      screen: Login,
    },
  },
  {
    headerMode: 'none',
  },
);

export const MainStackNavigator = StackNavigator({
  MainStack: {
    screen: SongsScreen,
  },
}, {
  headerMode: 'none',
  initialRouteName: 'MainStack',
});

// Drawer navigation
export const DrawerTab = TabNavigator({
  Songs: {
    screen: SongsDrawer,
  },
  SongLists: {
    screen: SongListsDrawer,
  },
}, {
  initialRouteName: 'Songs',
});

export const DrawerStack = StackNavigator({
  DrawerTab: {
    screen: DrawerTab,
  },
  SongListSongs: {
    screen: SongListSongsDrawer,
  },
}, {
  headerMode: 'none',
  cardStyle: {
    backgroundColor: 'white',
  },
});

// Create a component to avoid that 'contentComponent' pass props to DrawerTab
const DrawerTabComp = () => (<DrawerStack />);

export const MainNavigator = DrawerNavigator({
  Main: {
    screen: MainStackNavigator,
  },
}, {
  contentComponent: DrawerTabComp,
});

export const RootStackNavigator = StackNavigator({
  Main: {
    screen: MainNavigator,
  },
  SettingsModal: {
    screen: SettingsScreen,
  },
}, {
  mode: 'modal',
  headerMode: 'none',
});
